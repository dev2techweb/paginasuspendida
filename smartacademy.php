<?php 
/*
* Template Name: academy
*/
get_header(); ?>
<!-- hero principal -->
<!-- cambios en slider hacerlos en ambos :)-->
<div id="carouselExampleControls11" class="carousel slide hero__principal container__padding slider-1" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item full-height active">
        <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/BANNER1.png" alt="Third slide">
    
        </div>
        <div class="carousel-item full-height">
        <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/banner2.gif" alt="Third slide">
    
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls11" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls11" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<?php get_footer()?>