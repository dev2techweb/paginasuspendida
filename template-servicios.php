<?php
/*
* Template Name:  servicios
*/
get_header()
?>


  <section class=" w-50 e-center t-center page top">
  </section>
      <div class="full-width p-absolute franja_superior">
          <hr style="height:40px; background-color:#1fb5be;"/>
      </div>
      <div class=" full-width p-absolute titulo_superior">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 col-center">
              <div class="row">
                <div class="col-md-6" style="padding:0px;">
                    <h4 class="t-white" style="margin-top:20px;"><?php the_title(); ?></h4>
                </div>
                <div class="col-md-6">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="full-width p-absolute franja_inferior">
          <hr style="height:20px; background-color:#1fb5be;"/>
      </div>
  <div class="form__servicios container-fluid">
    <div class="row">
      <div class="col-md-6 col-center">
        <div class="row">
          <div class="col-md-6 p-relative" style="padding:0px;">
            <div class="descripcion_servicio">
              <?php
              echo get_post_meta($post->ID, 'descripcion', true);
              ?>
            </div>
            <div class="t-right j-center a-center mas_informacion">
              <p>
              <span style="font-weight: bold;">
                Mayor información
              </span>
              <i class="fa fa-arrow-right" style="font-size:4rem; vertical-align: middle; color:#259fdb"></i>
              </p>
            </div>
          </div>
            <div class="col-md-6">
              <button type="button" class="button_cotizar" onclick="mostrar()">Cotizar</button>
              <div id="demo" class="formulario_iphone">
                <div class="t-center item__container p-relative d-flex j-center a-center">
                  <figure class="item__img full-width full-height">
                    <a href="#serviceModal" data-toggle="modal" data-target="#serviceModal" servicio="Cargador nuevo - accesorios">
                      <img style="padding:0 !important;" src="<?php echo get_template_directory_uri(); ?>/assets/img/iphonex.png" alt="">
                    </a>
                  </figure>
                  <div class="p-absolute message_two full-width">
                    <div class="form__section" style="padding: 15%;">
                      <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                      <?php endwhile; endif; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  function mostrar(){
  document.getElementById('demo').style.display = 'block';}
  </script>
<?php get_footer()?>
