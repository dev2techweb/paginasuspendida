</main>
  <footer class="footer b-black t-white t-center item__padding">
    <span class="rights__text">Este sitio fue creado por <a href="https://techwebgt.com" class="text-white">TechWeb</a> - © <?php echo date('Y'); ?>
  </span>
  </footer>
  <?php wp_footer(); ?>
</body>
</html>
