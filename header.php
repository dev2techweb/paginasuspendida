<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php wp_title('-',true,'right'); ?><?php bloginfo('name');?></title>
  <?php wp_head(); ?>
</head>
<body>
  <header class="header b-black  t-center">
    <div class="imagen-moto" style="height:4rem;z-index:1;" onmouseover="ver()" onmouseout="ocultar()">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/moto.png" alt="" class="full-height">
          <div id="mensaje_moto" class="" style="position:absolute; top:100%; background-color:black; width:15rem; display:none; padding:1.2rem; padding-top: 2rem; color:white;margin-top:1rem;">
            Recogemos tu dispositivo a la puerta de tu casa u oficina
            <a role="button"  style="margin-top:1rem;" class="btn btn-info" href="<?php echo get_site_url(); ?>/contacto">Contáctanos</a>
          </div>
    </div>
    <div class="container-fluid" style="z-index:0;">
    <div class="e-center row j-center a-center">
      <section class="col-lg-3 logo d-flex j-center" style="z-index:3;">
        <div class="row">
          <div class="col-md-6">

          </div>
          <div class="col-md-6">
            <a href="<?php echo esc_url(home_url('/inicio'));?>">
              <figure class="logo__figure">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="">
              </figure>
            </a>
          </div>
        </div>
      </section>
      <nav class="navbar-expand-lg header__nav col-lg-6" style="z-index:1;">
        <button class="navbar-toggler e-center" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="fa fa-bars t-white"></span>
        </button>
          <?php wp_nav_menu(array(
            'theme_location' => 'principal',
            'container' => 'div',
            'container_class' => 'collapse navbar-collapse',
            'container_id' => 'navbarNav',
            'items_wrap' => '<ul class="navbar-nav e-center">%3$s</ul>',
            //clase del elemento li
            'menu_class' => 'nav-item'
          )); ?>
      </nav>
      <nav class="social__nav col-lg-3 d-flex j-end" style="z-index:2;">
        <ul class="nav">
          <div class="row clean">
            <div class="col clean">
              <li class="nav-item">
                <a href="https://www.instagram.com/smartservicegt/" class="nav-link t-white t-white__hover" target="_blank"><i class="fab fa-instagram"></i></a>
              </li>
              <li class="nav-item">
                <a href="https://www.facebook.com/SmartService-1554850278122848/" class="nav-link t-white t-white__hover" target="_blank"><i class="fab fa-facebook-square"></i></a>
              </li>
            </div>
          </div>
          <div class="row clean">
            <div class="col clean">
              <li class="nav-item"><a href="https://twitter.com/smart_servicegt?s=17" class=" nav-link t-white t-white__hover" target="_blank"><i class="fab fa-twitter"></i></a></li>
              <li class="nav-item">
                <a href="https://www.youtube.com/channel/UCZxpgWuqPKHi6dVeyAenPwQ" class="nav-link t-white t-white__hover" target="_blank"><i class="fab fa-youtube"></i></a>
              </li>
            </div>
          </div>
          <div class="row clean a-center j-center t-center">
            <div class="col clean">
              <li><a href="https://waze.com/ul/h9fxeh1nu9" class="t-white t-white__hover" target="_blank"><i class="fas fa-map-marker-alt icono-ubicacion"></i></a></li>
            </div>
          </div>
        </ul>
      </nav>
    </div>
    </div>
  </header>
  <main class="main">
    <script type="text/javascript">
    function ver(n) {
             document.getElementById("mensaje_moto").style.display="block"
             }
    function ocultar(n) {
             document.getElementById("mensaje_moto").style.display="none"
             }
    </script>
